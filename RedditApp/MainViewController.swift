//
//  ViewController.swift
//  RedditApp
//
//  Created by Jake Sulkoske on 1/19/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import UIKit
import LBTAComponents

class MainViewController: DatasourceController {
    
    private let redditUrlString = "https://www.reddit.com/r/pics/.json?count=5"
    private var mainDatasource = MainDatasource();
    private var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Reddit Pics"
        
        //Get data when view loads
        getDataFromReddit()
        
        //Set Up collection View
        self.datasource = mainDatasource
        
        //Pull to refresh implementation
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(getDataFromReddit), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
    }
    
    @objc func getDataFromReddit(){
        guard let redditUrl = URL(string: redditUrlString) else { return }
        URLSession.shared.dataTask(with: redditUrl) { data, response, error in
            guard let data = data else { return }
            do {
                let postObjects = try JSONDecoder().decode(Data.self, from: data)
                DispatchQueue.main.async {
                    self.mainDatasource.postObjects = postObjects.data.children
                    self.collectionView?.reloadData()
                    self.refreshControl.endRefreshing()
                }
            } catch let err{
                print(err)
            }
        }.resume()
        
    }
    
    //Set sizes for main cell, header, and footer
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let postObject = self.datasource?.item(indexPath) as? Children {
            
            //calculate image width
            let imageWidth = postObject.data.preview.images[0].source.width
            let imageHeight = postObject.data.preview.images[0].source.height
            let scale = view.frame.width / CGFloat(imageWidth)
            
            //let newImageWidth = CGFloat(imageWidth) * scale
            let newImageHeight = CGFloat(imageHeight) * scale
            let height = 32 + 20 +  CGFloat(newImageHeight)
            
            //Cell size for label
            let postText = postObject.data.title
            let approximatWidthofText = view.frame.width - 24
            let size = CGSize(width: approximatWidthofText, height: 1000)
            let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)]
            let estimatedFrame = NSString(string: postText!).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes:attributes, context: nil)
            
            let testHeight = estimatedFrame.height + height
            
            return CGSize(width: view.frame.width, height: testHeight)
            
        }
        return CGSize(width: view.frame.width, height: 600)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
}

