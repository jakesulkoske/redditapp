//
//  PostCell.swift
//  RedditApp
//
//  Created by Jake Sulkoske on 1/19/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import UIKit
import Foundation
import LBTAComponents

class PostCell: DatasourceCell{
    
    override var datasourceItem: Any? {
        didSet {
            
            guard let post = datasourceItem as? Children else { return }
            
            //Set post subviews with post data
            postAuthorLabel.text = "@" + post.data.author!
            postTextLabel.text = post.data.title
            postImageView.getPostImagesAsync(urlString: post.data.preview.images[0].source.url!)
        }
    }
    
    let postAuthorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    let postImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 0.5)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let postTextLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 0.5)
        
        //Add subviews to cell
        addSubview(postAuthorLabel)
        addSubview(postImageView)
        addSubview(postTextLabel)
        
        //Set anchors for cell subviews
        postAuthorLabel.anchor(self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 8, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        postImageView.anchor(postAuthorLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        postTextLabel.anchor(postImageView.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 8, leftConstant: 12, bottomConstant: 8, rightConstant: 12, widthConstant: 0, heightConstant: 0)
    }
}
