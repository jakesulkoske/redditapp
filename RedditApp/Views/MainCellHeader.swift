//
//  MainCellHeader.swift
//  RedditApp
//
//  Created by Jake Sulkoske on 1/19/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import UIKit
import Foundation
import LBTAComponents

class MainCellHeader: DatasourceCell {
    
    let textLabel: UILabel = {
        let label = UILabel()
        label.text = "Posts from r/pics"
        label.font = UIFont.systemFont(ofSize: 16)
        
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(textLabel)
        
        textLabel.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}
