//
//  MainDataSource.swift
//  RedditApp
//
//  Created by Jake Sulkoske on 1/19/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import Foundation
import LBTAComponents

class MainDatasource: Datasource {
    var postObjects = [Children]()
    
    override func headerClasses() -> [DatasourceCell.Type]? {
        return [MainCellHeader.self]
    }
    
    override func footerClasses() -> [DatasourceCell.Type]? {
        return [MainCellFooter.self]
    }
    
    override func numberOfItems(_ section: Int) -> Int {
        return postObjects.count
    }
    
    override func item(_ indexPath: IndexPath) -> Any? {
        return postObjects[indexPath.item]
    }
    
    override func cellClasses() -> [DatasourceCell.Type] {
        return [PostCell.self]
    }
    
}
