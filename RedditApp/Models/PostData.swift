//
//  PostData.swift
//  RedditApp
//
//  Created by Jake Sulkoske on 1/19/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import Foundation

struct PostData: Decodable{
    let id: String?
    let thumbnail: String?
    let title: String?
    let author: String?
    let preview: Preview
}
