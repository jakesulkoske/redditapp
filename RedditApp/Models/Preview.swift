//
//  Preview.swift
//  RedditApp
//
//  Created by Jake Sulkoske on 1/19/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import Foundation

struct Preview: Decodable {
    let images: [PostImages]
}
