//
//  Extensions.swift
//  RedditApp
//
//  Created by Jake Sulkoske on 1/19/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import Foundation

import UIKit

let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
    
    public func getPostImagesAsync(urlString: String) {
        
        //Set cell image to nil so that images are not re-used in cells
        self.image = nil
        
        //Initial check to see if thumbnail image is in cache
        if let cachedThumbnail = imageCache.object(forKey: urlString as NSString) as? UIImage{
            self.image = cachedThumbnail
            return
        }
        
        //Retrieve thumbnail asynchronously
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                if let thumbnailImage = UIImage(data: data!){
                    imageCache.setObject(thumbnailImage, forKey: urlString as NSString)
                    self.image = thumbnailImage
                }
            })
            
        }).resume()
    }
}
